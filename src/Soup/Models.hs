{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}

module Soup.Models 
  ( Item(..)
  , Tag(..)
  , _item
  , _tag
  , Tagging
  , Parser
  , ParseError
  , Value(..)
  , FromValue(..)
  , parseValue
  , parseItem
  , parseItems
  , parseTag
  , parseTags
  , mkTagging
  , mkTaggings) where




data Value =
  VItem Item
  | VItems [Item]
  | VTag Tag
  | VTags [Tag]
  deriving (Eq, Show)

class ToValue a where
  val :: a -> Value

class FromValue a where
  unval :: Value -> a


type ParseError = String
type Parser s a = s -> Either ParseError a

parseValue :: (ToValue a) => Parser s a
           -> s -> Either ParseError Value
parseValue parser s = fmap val (parser s)


----------------- ITEM --
data Item = Item String
  deriving (Eq)
 
parseItem :: Parser String Item
parseItem "" = Left "item cannot be empty"
parseItem s = Right (Item s)

parseItems :: Parser [String] [Item]
parseItems xs = Right (map Item xs)


instance Show Item where
  show (Item s) = show s

instance ToValue Item where val = VItem
instance ToValue [Item] where val = VItems
instance FromValue Item where
  unval (VItem i) = i
instance FromValue [Item] where
  unval (VItems is) = is



----------------- TAG --
data Tag = Tag String
  deriving (Eq)

parseTag :: Parser String Tag
parseTag "" = Left "tag cannot be empty"
parseTag s = Right (Tag s)

parseTags :: Parser [String] [Tag]
parseTags xs = Right (map Tag xs)

instance Show Tag where
  show (Tag s) = show s

instance ToValue Tag where val = VTag
instance ToValue [Tag] where val = VTags
instance FromValue Tag where
  unval (VTag t) = t
instance FromValue [Tag] where
  unval (VTags ts) = ts



----------------- TAGGING --
data Tagging =
  Tagging { _item :: String
          , _tag :: String }
  deriving (Eq)

instance Show Tagging where
  show Tagging{..} = show (_item, _tag)

mkTagging :: Item -> Tag -> Tagging
mkTagging (Item item) (Tag tag) = Tagging item tag

mkTaggings :: Item -> [Tag] -> [Tagging]
mkTaggings item tags = map (mkTagging item) tags

{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}

module Soup.Types where

import Control.Monad.State

import Data.Maybe (fromMaybe)

import Soup.Models



-- param types
data PTag
data PItem

class SoupType a
instance SoupType PTag
instance SoupType PItem
instance SoupType [PTag]
instance SoupType [PItem]

class (SoupType a) => SoupSingle a
instance SoupSingle PTag
instance SoupSingle PItem

class (SoupType a) => SoupList a
instance SoupList [PTag]
instance SoupList [PItem]


-- labels
data Labeled
data Unlabeled

-- | A parameter that gets passed in from the CLI.
data Param a t where
  Param :: (SoupSingle t) => String -> Param Unlabeled t
  Params :: (SoupList t) => String -> Param Unlabeled t
  -- it's impossible to label a label! ;P
  LblParam :: String
           -> Param Unlabeled t
           -> Param Labeled t


-- | Remove a parameter's label, if any is present,
-- thus changing its type to @Unlabeled@.
unlabel :: (SoupType t) => Param a t
        -> (Maybe String, Param Unlabeled t)
unlabel param =
  case param of
    LblParam a p -> (Just a, p)
    Param  _ -> (Nothing, param)
    Params _ -> (Nothing, param)

paramName :: Param a t -> String
paramName param =
  case param of
    LblParam _ p -> paramName p
    Param  p -> p
    Params p -> p

-- | A wrapped param collapses a @Param@ into a single
-- type, provided that the @Param@ in question has
-- one of the standard soup types.
type WParam = (Maybe String, WParam')
data WParam' =
  WItem (Param Unlabeled PItem)
  | WItems (Param Unlabeled [PItem])
  | WTag (Param Unlabeled PTag)
  | WTags (Param Unlabeled [PTag])

wparamName :: WParam' -> String
wparamName wparam =
  case wparam of
    WItem  p -> paramName p
    WItems p -> paramName p
    WTag   p -> paramName p
    WTags  p -> paramName p
    
class (SoupType t) => Wrappable t where
  wrap :: Param Unlabeled t -> WParam'

instance Wrappable PItem where wrap = WItem
instance Wrappable [PItem] where wrap = WItems
instance Wrappable PTag where wrap = WTag
instance Wrappable [PTag] where wrap = WTags

wrapParam :: (Wrappable t) => Param a t -> WParam
wrapParam p = (l, w)
  where (l, p') = unlabel p
        w = wrap p'



-- more or less a @Param@ with no need for labels
-- | A named, typed variable in Soup's EDSL.
data Variable t where
  Var :: (SoupType t) => String -> Variable t

-- | Create a @Variable@ from a @Parameter@.
var :: (SoupType t) => Param a t -> Variable t
var (Param s) = Var s
var (Params s) = Var s
var (LblParam _ p) = var p

-- | Get a variable's name.
varName :: Variable t -> String
varName (Var s) = s


data Instr =
  Create (Variable PItem) (Variable [PTag])
  | RmItems (Variable [PItem])
  | RmTags (Variable [PTag])
  | Untag (Variable PItem) (Variable [PTag])
  | LsTags (Variable [PItem])
  | LsItems (Variable [PTag])
  | Vomit -- show all database contents


-- | A command written in the soup EDSL.
data Command = Command
  { _name :: [String]
  , _desc :: String
  , _params :: [WParam]
  , _instrs :: [Instr] }

newCommand :: Command
newCommand =
  Command { _name = []
          , _desc = []
          , _params = []
          , _instrs = [] }

-- | Generate a subcommand's documentation.
doc :: Command -> String
doc c = unwords (name:params) ++ ": " ++ desc
  where name = unwords (_name c)
        desc = _desc c
        params = map strparam (_params c)
        strparam (lbl, p) =
          fromMaybe "" (fmap ((++" ") . ("--"++)) lbl)
          ++ wparamName p


type Soup a = State Command a

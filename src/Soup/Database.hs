{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}

module Soup.Database
  ( SoupQuery
  , withDatabase
  , addTagging
  , allTaggings
  , removeItem
  , removeTag
  , removeTagging
  , getTags
  , getItems ) where

import Data.List (intercalate)

import Database.HDBC
import Database.HDBC.Sqlite3
import Data.Convertible (Convertible)

import Control.Monad
import Control.Monad.Reader

import Soup.Models


type SoupQuery a = ReaderT Connection IO a

-- | Do something with the tagging database. This
-- will create the database and/or tag table if it
-- doesn't already exist.
withDatabase :: FilePath -> SoupQuery a -> IO a
withDatabase db q = do
  conn <- connectSqlite3 db
  run conn mktab []
  a <- runReaderT q conn
  commit conn
  disconnect conn
  return a
  where mktab = "CREATE TABLE IF NOT EXISTS \
                \ taggings(_item TEXT, _tag TEXT);"


-- | Add a tagging to the database. Duplicates are
-- not added.
addTagging :: Tagging -> SoupQuery Integer
addTagging t = doQuery query [item, tag, item, tag]
  where query = "INSERT INTO taggings (_item, _tag) \
                \ SELECT ?, ? WHERE NOT EXISTS ( \
                \ SELECT 1 FROM taggings t WHERE \
                \ t._item=? AND t._tag=?);"
        item = _item t
        tag = _tag t

-- | Get _every_ tagging from the database.
allTaggings :: SoupQuery [Tagging]
allTaggings = do
  rows <- doQuickQuery query ([] @String)
  return (map row2Tagging rows)
  where query = "SELECT * FROM taggings;"

-- | Get all the tags that the given items have.
-- No arguments will get every tag.
getTags :: [Item] -> SoupQuery [Tag]
getTags is = do
  rows <- doQuickQuery query ([] @String)
  return (map row2Tag rows)
  where q1 = "SELECT DISTINCT _tag FROM taggings \
             \ WHERE _item IN ("
             ++ intercalate "," (map show is) ++ ")"
        q2 = "SELECT DISTINCT _tag FROM taggings;"
        query = if null is then q2 else q1

-- | Get all the items that have any of the given tags.
-- No arguments will get every item.
getItems :: [Tag] -> SoupQuery [Item]
getItems ts = do
  rows <- doQuickQuery query ([] @String)
  return (map row2Item rows)
  where q1 = "SELECT DISTINCT _item FROM TAGGINGS \
             \ WHERE _tag IN ("
             ++ intercalate "," (map show ts) ++ ")"
        q2 = "SELECT DISTINCT _item FROM taggings;"
        query = if null ts then q2 else q1
  


-- | Remove an item.
removeItem :: Item -> SoupQuery Integer
removeItem (Item i) = doQuery query [i]
  where query = "DELETE FROM taggings WHERE _item=?;"

-- | Remove a tag.
removeTag :: Tag -> SoupQuery Integer
removeTag (Tag t) = doQuery query [t]
  where query = "DELETE FROM taggings WHERE _tag=?;"

-- | Remove a tagging.
removeTagging :: Tagging -> SoupQuery Integer
removeTagging t = doQuery query [_item t, _tag t]
  where query = "DELETE FROM taggings WHERE \
                \ _item=? AND _tag=?;"


--------------------- util ---

-- | Convert an SQL row to a @Tagging@.
row2Tagging :: [SqlValue] -> Tagging
row2Tagging [item, tag] =
  mkTagging (Item (fromSql item)) (Tag (fromSql tag))
row2Tagging _ =
  error "<internal error @r2; please report>"

-- | Convert an SQL row to a @Tag@.
row2Tag :: [SqlValue] -> Tag
row2Tag [tag] = Tag (fromSql tag)
row2Tag _ =
  error "<internal error @r2; please report>"

-- | Convert an SQL row to an @Item@.
row2Item :: [SqlValue] -> Item
row2Item [item] = Item (fromSql item)
row2Item _ =
  error "<internal error @r2; please report>"

-- | Run a query, getting the connection from the
-- reader monad.
doQuery :: (Convertible a SqlValue)
        => String -> [a] -> SoupQuery Integer
doQuery q args = do
  conn <- ask
  liftIO $ run conn q (map toSql args)

doQuery_ :: (Convertible a SqlValue)
         => String -> [a] -> SoupQuery ()
doQuery_ q args = doQuery q args >> return ()


-- | Like @doQuery@, but runs @quickQueries@.
doQuickQuery :: (Convertible a SqlValue)
             => String -> [a] -> SoupQuery [[SqlValue]]
doQuickQuery q args = do
  conn <- ask
  liftIO $ quickQuery' conn q (map toSql args)

-- | Helper functions for Soup commands.
module Soup.Util
  ( name
  , desc
  , tag
  , tags
  , item
  , items
  , ltag
  , ltags
  , litem
  , litems
  , tagItem
  , rmItems
  , rmTags
  , lsTags
  , lsItems
  , untag
  , vomit ) where

import Control.Monad.State

import Soup.Types
import Soup.Models
import Soup.Database


-- metadata setters
name :: String -> Soup ()
name n = modify (\s -> s{_name = words n})

desc :: String -> Soup ()
desc d = modify (\s -> s{_desc = d})


tag :: String -> Soup (Param Unlabeled PTag)
tag s = addParam (Param s)

item :: String -> Soup (Param Unlabeled PItem)
item s = addParam (Param s)

tags :: String -> Soup (Param Unlabeled [PTag])
tags s = addParam (Params s)

items :: String -> Soup (Param Unlabeled [PItem])
items s = addParam (Params s)

ltag :: String -> String -> Soup (Param Labeled PTag)
ltag l s = addParam (LblParam l (Param s))

litem :: String -> String -> Soup (Param Labeled PItem)
litem l s = addParam (LblParam l (Param s))

ltags :: String -> String -> Soup (Param Labeled [PTag])
ltags l s = addParam (LblParam l (Params s))

litems :: String -> String -> Soup (Param Labeled [PItem])
litems l s = addParam (LblParam l (Params s))



tagItem :: Param a PItem -> Param b [PTag] -> Soup ()
tagItem i ts = emit (Create (var i) (var ts))

rmItems :: Param a [PItem] -> Soup ()
rmItems is = emit (RmItems (var is))

rmTags :: Param a [PTag] -> Soup ()
rmTags ts = emit (RmTags (var ts))

untag :: Param a PItem -> Param a [PTag] -> Soup ()
untag i ts = emit (Untag (var i) (var ts))

lsTags :: Param a [PItem] -> Soup ()
lsTags is = emit (LsTags (var is))

lsItems :: Param a [PTag] -> Soup ()
lsItems ts = emit (LsItems (var ts))

-- a cruel angel's emesis
vomit :: Soup ()
vomit = emit Vomit
  

-- other util stuff
addParam :: (Wrappable t) => Param a t -> Soup (Param a t)
addParam p = do
  let p' = wrapParam p
  modify (\s -> s{_params = (_params s) ++ [p']})
  return p

emit :: Instr -> Soup ()
emit i =
  modify (\s -> s{_instrs = (_instrs s) ++ [i]})

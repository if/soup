module Soup.Interpreter.ParamParser
  ( Binding
  , Args
  , parseParams ) where

import Control.Monad
import Data.List

import Soup.Models
import Soup.Types


type Args = [String]
type Binding = (String, Value)

parseParams :: Command -> Args
            -> Either ParseError [Binding]
parseParams cmd args =
  fst <$> foldM bindParam init params
  where params = _params cmd
        init = ([] :: [Binding], args)

bindParam :: ([Binding], Args) -> WParam
           -> Either ParseError ([Binding], Args)
bindParam (bs, args) param = do
  (binding, args') <- expectArg param args
  return (binding : bs, args')

expectArg :: WParam -> Args
          -> Either ParseError (Binding, Args)
expectArg (lbl, param) args = do
  args' <- parseLabel lbl args
  (val, args'') <- parseParam param args'
  let binding = mkBinding param val
  return (binding, args'')

parseLabel :: Maybe String -> Args -> Either ParseError Args
parseLabel Nothing args = Right args
parseLabel (Just lbl) [] =
  Left ("expected flag: --" ++ lbl)
parseLabel (Just lbl) (l:args') =
  if ("--"++lbl) == l then Right args'
  else Left ("expected flag: --" ++ lbl)

parseParam :: WParam' -> Args
           -> Either ParseError (Value, Args)
parseParam p@(WItem _) [] =
  Left ("expected argument for param `"
         ++ wparamName p ++ "'")           
parseParam p@(WTag _) [] =
  Left ("expected argument for param `"
         ++ wparamName p ++ "'")
parseParam param args =
  case param of
    WItem  _ -> parseHead parseItem
    WTag   _ -> parseHead parseTag
    WItems _ -> parseMany parseItems
    WTags  _ -> parseMany parseTags
  where parseHead f = do
          let Just (a, as) = uncons args
          val <- parseValue f a
          return (val, as)
        parseMany f = do
          let (as, as') = break isFlag args
          val <- parseValue f as
          return (val, as')
        isFlag = isPrefixOf "--"

mkBinding :: WParam' -> Value -> Binding
mkBinding p val = (wparamName p, val)

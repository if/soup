{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Soup.Interpreter (runSoup) where

import Control.Monad
import Control.Monad.Reader
import Control.Monad.State (execState)

import Data.List
import Data.Maybe (fromMaybe)
import Data.Ord (comparing)

import Soup.Database
import Soup.Models
import Soup.Types
import Soup.Interpreter.ParamParser


-- | Run the soup CLI, given a list of command-line
-- arguments and a list of named subcommands.
runSoup :: [Soup a] -> Args -> IO ()
runSoup cmds args =
  runSoup' (map compile cmds) args
  where compile c = execState c newCommand


runSoup' :: [Command] -> Args -> IO ()
runSoup' cmds [] = help cmds
runSoup' cmds args = do
  case findMatch args cmds of
    Nothing -> help cmds
    Just c -> run c args

-- | Interpret a command, given the arguments
-- passed to it.
run :: Command -> Args -> IO ()
run c args = do
  let Just args' = stripPrefix (_name c) args
  case parseParams c args' of
    Left  err -> putStrLn ("*** error: " ++ err)
    Right ctx -> interpret (_instrs c) ctx

-- | Interpret a command given variable bindings.
interpret :: [Instr] -> [Binding] -> IO ()
interpret instrs ctx = mapM_ doReader instrs
  where doReader i = runReaderT (interpret' i) ctx


-- | Interpret a single instruction within the context
-- of variable bindings.
interpret' :: Instr -> ReaderT [Binding] IO ()
interpret' (Create a b) = do
  item <- resolve' a
  tags <- resolve' b
  let ts = mkTaggings item tags
  dbWithLog (fmtPrefix "added") addTagging ts
interpret' Vomit = do
    taggings <- db allTaggings
    liftIO (mapM_ print taggings)
interpret' (RmItems a) = do
  items :: [Item] <- resolve' a
  dbWithLog (fmtPrefix "removed") removeItem items
interpret' (RmTags a) = do
  tags :: [Tag] <- resolve' a
  dbWithLog (fmtPrefix "removed") removeTag tags
interpret' (Untag a b) = do
  item <- resolve' a
  tags <- resolve' b
  let ts = mkTaggings item tags
  dbWithLog (fmtPrefix "removed") removeTagging ts
interpret' (LsTags a) = do
  items :: [Item] <- resolve' a
  tags <- db (getTags items)
  liftIO $ forM_ tags \case
    Tag t -> putStrLn t
interpret' (LsItems a) = do
  tags :: [Tag] <- resolve' a
  items <- db (getItems tags)
  liftIO $ forM_ items \case
    Item i -> putStrLn i

-- | Resolve a variable's value. Note that this will
-- throw an error for a variable that can't be found.
resolve :: (Monad m) => Variable t
        -> ReaderT [Binding] m Value
resolve v = do
  let s = varName v
  res <- asks (lookup s)
  return (unwrap res)
  where unwrap = fromMaybe $
          error "<internal error @re; please report>"

-- | Like @resolve@, but "unvalues" the result.
resolve' :: (Monad m, FromValue v) => Variable t
         -> ReaderT [Binding] m v
resolve' = fmap unval . resolve


-- | Display a CLI usage message based on a
-- list of commands.
help :: [Command] -> IO ()
help cmds = do
  let subs = map doc cmds
  putStrLn "command documentation:"
  forM_ subs $ \h -> do
    putStr "  "
    putStrLn h


-- | Find the command whose name is the maximally long
-- match to the list of arguments.
findMatch :: Args -> [Command] -> Maybe Command
findMatch args cmds = find (isMatch args) cmds'
  where cmds' = sortBy nameLength cmds
        nameLength = comparing (length . _name)
        isMatch as c = _name c `isPrefixOf` as


-- | Do something with the "soup.db" database.
db :: SoupQuery a -> ReaderT [Binding] IO a
db = liftIO . withDatabase "soup.db"

-- | A specialized version of @db@ that works on a list
-- of items, and logs what happens to each item.
dbWithLog :: (a -> String)
          -> (a -> SoupQuery Integer)
          -> [a] -> ReaderT [Binding] IO ()
dbWithLog fmt query xs = do
  ns <- db (mapM (keepM query) xs)
  liftIO $ forM_ ns \case
    (_, 0) -> return ()
    (x, _) -> putStrLn (fmt x)

fmtPrefix :: (Show a) => String -> a -> String
fmtPrefix prefix a = prefix ++ " " ++ show a

-- do a monadic action on `a',
-- returning `a' with the result
keepM :: (Monad m) => (a -> m b) -> a -> m (a, b)
keepM m a = m a >>= \b -> return (a, b)

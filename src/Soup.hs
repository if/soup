module Soup (runCLI) where

import Soup.Types
import Soup.Models
import Soup.Util
import Soup.Interpreter


runCLI :: [String] -> IO ()
runCLI args = runSoup commands args
  where commands = [ tag_
                   , lsTags_
                   , lsItems_
                   , rmItem_
                   , rmTags_
                   , untag_
                   , vomit_ ]


tag_ :: Soup ()
tag_ = do
  name "tag"
  desc "associate an item with tags."
  it  <- item "ITEM"
  tgs <- tags "TAGS"
  tagItem it tgs

lsTags_ :: Soup ()
lsTags_ = do
  name "ls tags"
  desc "list tags of each item."
  is <- items "ITEMS"
  lsTags is

lsItems_ :: Soup ()
lsItems_ = do
  name "ls items"
  desc "list items under each tag."
  ts <- tags "TAGS"
  lsItems ts

rmItem_ :: Soup ()
rmItem_ = do
  name "rm items"
  desc "remove an item."
  its <- items "ITEMS"
  rmItems its

rmTags_ :: Soup ()
rmTags_ = do
  name "rm tags"
  desc "remove a tag."
  ts <- tags "TAGS"
  rmTags ts

untag_ :: Soup ()
untag_ = do
  name "untag"
  desc "remove tags from an item."
  it  <- item "ITEM"
  tgs <- tags "TAGS"
  untag it tgs

vomit_ :: Soup ()
vomit_ = do
  name "vomit"
  desc "print all database contents."
  vomit

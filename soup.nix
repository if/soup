{ mkDerivation, base, convertible, exceptions, HDBC, HDBC-sqlite3
, mtl, stdenv, transformers
}:
mkDerivation {
  pname = "soup";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base convertible exceptions HDBC HDBC-sqlite3 mtl transformers
  ];
  description = "A CLI application for tagging files";
  license = stdenv.lib.licenses.gpl3;
}

-- set up the soup database.
-- run `sqlite3 soup.db` and then `.read init.sql` in the console.
-- create table taggings(_id INT AUTO_INCREMENT PRIMARY KEY, _item TEXT, _tag TEXT);
CREATE TABLE IF NOT EXISTS taggings(_item TEXT, _tag TEXT);

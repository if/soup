 let pkgs = import <nixpkgs> {};
in {
  soup = pkgs.haskellPackages.callPackage ./soup.nix {};
}

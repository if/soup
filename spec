create table items(itemName TEXT PRIMARY KEY);
create table tags(tagName TEXT PRIMARY KEY);
create table taggings(tgID INT AUTO_INCREMENT PRIMARY KEY, tgItem TEXT, tgTag TEXT);

soup spec
---------
tag:
ls - list all tags
ls ITEM - list tags of item
assoc ITEM TAGS - tag item
rm TAGS - remove tags
rmfrom ITEM TAGS - remove tags from item

item:
ls - list all items
ls TAGS - list items with some given tags (union)
rm ITEMS - remove items from db
ls -i TAGS - list items with all given tags (intersection)

types of output:
- list-like
  - consists of title and items
    items tagged "soup" and "folder":
    * soup_pictures/
    * ./
- "feedback" message
  - series of simple one-liners
  deleted tag "soup"
  deleted tag "cs"

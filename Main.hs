module Main where

import Soup
import System.Environment (getArgs)


main :: IO ()
main = getArgs >>= runCLI
